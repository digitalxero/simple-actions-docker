package docker

import (
	"os"
	"testing"

	"gitlab.com/digitalxero/simple-actions"
	"gitlab.com/digitalxero/simple-actions/term"
	"gitlab.com/digitalxero/simple-actions/log"
)

func TestPullAction(t *testing.T) {
	testCtx := actions.NewDefaultActionContext(term.NewLogger(os.Stderr, log.Level(3)), os.Getenv("CI") != "")
	action := NewPullImageAction("dieterreuter/hello")
	if err := action.Execute(testCtx); err != nil {
		t.Fatal(err)
	}
}
