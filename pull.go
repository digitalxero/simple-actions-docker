package docker

import (
	"fmt"

	"github.com/docker/docker/client"
	"gitlab.com/digitalxero/simple-actions"
)

type pullImageAction struct {
	image string
}

// NewPullImageAction docs.
func NewPullImageAction(image string) actions.Action {
	return &pullImageAction{image: image}
}

// Execute docs.
func (a *pullImageAction) Execute(ctx actions.ActionContext) (err error) {
	var cli *client.Client
	defer func() {
		ctx.Status().End(err == nil)
	}()
	if cli, err = baseExecute(ctx.Status(), fmt.Sprintf("Ensuring docker image (%s)", a.image)); err != nil {
		return err
	}

	if ctx.IsDryRun() {
		return nil
	}

	err = pullImage(ctx.Logger(), cli, a.image)

	return err
}
