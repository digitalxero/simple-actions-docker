package docker

import (
	"context"
	"fmt"
	"io/ioutil"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"gitlab.com/digitalxero/simple-actions"
)

type execAction struct {
	name,
	cmd string
	args []string
}

// NewExecAction docs.
func NewExecAction(name, cmd string, args []string) actions.Action {
	return &execAction{name: name, cmd: cmd, args: args}
}

// Execute docs.
func (a *execAction) Execute(ctx actions.ActionContext) (err error) {
	var (
		cli       *client.Client
		dockerCtx = context.Background()
		execInfo  types.IDResponse
		resp      types.HijackedResponse
		data      []byte
	)

	defer func() {
		ctx.Status().End(err == nil)
	}()
	if cli, err = baseExecute(ctx.Status(), fmt.Sprintf("Executing %s on %s", a.cmd, a.name)); err != nil {
		return err
	}

	if ctx.IsDryRun() {
		return nil
	}

	cmd := []string{a.cmd}
	cmd = append(cmd, a.args...)
	if execInfo, err = cli.ContainerExecCreate(dockerCtx, a.name, types.ExecConfig{
		Tty: true,
		Cmd: cmd,
	}); err != nil {
		return err
	}

	if resp, err = cli.ContainerExecAttach(dockerCtx, execInfo.ID, types.ExecStartCheck{Tty: true}); err != nil {
		return err
	}
	defer resp.Close()

	if data, err = ioutil.ReadAll(resp.Reader); err != nil {
		return err
	}

	ctx.Logger().V(3).Info(string(data))

	return err
}
