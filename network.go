package docker

import (
	"context"
	"fmt"
	"strings"

	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/client"
	"gitlab.com/digitalxero/simple-actions"
)

type networkConnectAction struct {
	networkName,
	containerName string
}

// NewNetworkConnectAction docs.
func NewNetworkConnectAction(networkName, containerName string) actions.Action {
	return &networkConnectAction{networkName: networkName, containerName: containerName}
}

// Execute docs.
func (a *networkConnectAction) Execute(ctx actions.ActionContext) (err error) {
	var (
		cli       *client.Client
		dockerCtx = context.Background()
	)

	defer func() {
		ctx.Status().End(err == nil)
	}()
	if cli, err = baseExecute(ctx.Status(), fmt.Sprintf("Connecting %s to container %s", a.networkName, a.containerName)); err != nil {
		return err
	}

	if ctx.IsDryRun() {
		return nil
	}

	if err = cli.NetworkConnect(
		dockerCtx,
		a.networkName,
		a.containerName,
		&network.EndpointSettings{}); err != nil && strings.Contains(err.Error(), "already exists") {
		return nil
	}

	return err
}
