package docker

import (
	"context"
	"fmt"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"gitlab.com/digitalxero/simple-actions"
)

type removeAction struct {
	name  string
	force bool
}

// NewRemoveAction docs.
func NewRemoveAction(name string, force bool) actions.Action {
	return &removeAction{name: name, force: force}
}

// Execute docs.
func (a *removeAction) Execute(ctx actions.ActionContext) (err error) {
	var (
		cli       *client.Client
		dockerCtx = context.Background()
	)

	defer func() {
		ctx.Status().End(err == nil)
	}()
	if cli, err = baseExecute(ctx.Status(), fmt.Sprintf("Removing container %s", a.name)); err != nil {
		return err
	}

	if ctx.IsDryRun() {
		return nil
	}

	if err = cli.ContainerRemove(dockerCtx, a.name, types.ContainerRemoveOptions{
		RemoveLinks:   false,
		RemoveVolumes: true,
		Force:         a.force,
	}); err != nil && strings.Contains(err.Error(), "No such container") {
		// if the container does not exist, then it is obviously removed.
		ctx.Logger().V(3).Info(err.Error())
		err = nil
	}

	return err
}
