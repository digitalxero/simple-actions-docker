package docker

import (
	"context"
	"fmt"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
	"github.com/drone/envsubst"
	"gitlab.com/digitalxero/simple-actions"
)

type runImageAction struct {
	name,
	image string
	env,
	volumes,
	ports,
	entrypoint,
	cmd []string
	restart bool
}

// NewRunImageAction docs.
func NewRunImageAction(image, name string, env, ports, volumes, entrypoint, cmd []string, restart bool) actions.Action {
	return &runImageAction{
		image:      image,
		name:       name,
		env:        env,
		ports:      ports,
		volumes:    volumes,
		entrypoint: entrypoint,
		cmd:        cmd,
		restart:    restart,
	}
}

// Execute docs
// nolint:gocognit
func (a *runImageAction) Execute(ctx actions.ActionContext) (err error) {
	var (
		cli          *client.Client
		containerID  string
		resp         container.ContainerCreateCreatedBody
		imageInfo    types.ImageInspect
		found        []types.Container
		dockerCtx    = context.Background()
		inspectResp  []byte
		containerEnv []string
	)

	defer func() {
		ctx.Status().End(err == nil)
	}()
	if cli, err = baseExecute(ctx.Status(), fmt.Sprintf("Starting %s", a.name)); err != nil {
		return err
	}

	if ctx.IsDryRun() {
		return nil
	}

	if imageInfo, inspectResp, err = cli.ImageInspectWithRaw(dockerCtx, a.image); err != nil && client.IsErrNotFound(err) {
		if err = pullImage(ctx.Logger(), cli, a.image); err != nil {
			return err
		}
		imageInfo, inspectResp, err = cli.ImageInspectWithRaw(dockerCtx, a.image)
	}

	if err != nil {
		if inspectResp != nil {
			ctx.Logger().V(3).Info(string(inspectResp))
		}
		return err
	}

	if imageInfo.Config != nil {
		containerEnv = append(containerEnv, imageInfo.Config.Env...)
		if a.entrypoint == nil {
			a.entrypoint = imageInfo.Config.Entrypoint
		}
		if a.cmd == nil {
			a.cmd = imageInfo.Config.Cmd
		}
	}

	containerEnv = append(containerEnv, a.env...)
	for idx, eEnv := range containerEnv {
		if eEnv, err = envsubst.EvalEnv(eEnv); err == nil {
			containerEnv[idx] = eEnv
		}
	}

	containerConf := &container.Config{
		Image:      a.image,
		Env:        containerEnv,
		Entrypoint: a.entrypoint,
		Cmd:        a.cmd,
		StopSignal: "SIGTERM",
		OpenStdin:  true,
		StdinOnce:  true,
	}
	hostConf := &container.HostConfig{}

	if a.ports != nil {
		if containerConf.ExposedPorts, hostConf.PortBindings, err = nat.ParsePortSpecs(a.ports); err != nil {
			return err
		}
	}

	if a.restart {
		hostConf.RestartPolicy = container.RestartPolicy{Name: "always"}
	}

	if a.volumes != nil {
		for idx, vol := range a.volumes {
			if vol, err = envsubst.EvalEnv(vol); err == nil {
				a.volumes[idx] = vol
			}
		}
		hostConf.Binds = a.volumes
	}

	if found, err = cli.ContainerList(dockerCtx, types.ContainerListOptions{
		All:     true,
		Filters: filters.NewArgs(filters.KeyValuePair{Key: "name", Value: a.name}),
	}); err != nil {
		return err
	}

	if len(found) == 0 {
		if resp, err = cli.ContainerCreate(dockerCtx, containerConf, hostConf, nil, nil, a.name); err != nil {
			return err
		}
		containerID = resp.ID
	} else {
		containerID = found[0].ID
		if found[0].State == "running" {
			return nil
		}
	}

	err = cli.ContainerStart(dockerCtx, containerID, types.ContainerStartOptions{})

	return err
}
