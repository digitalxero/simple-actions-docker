package docker

import (
	"context"
	"fmt"

	"github.com/docker/docker/client"
	"gitlab.com/digitalxero/simple-actions"
)

type tagImageAction struct {
	oldImage, newImage string
}

// NewTagImageAction docs.
func NewTagImageAction(oldImage, newImage string) actions.Action {
	return &tagImageAction{
		oldImage: oldImage,
		newImage: newImage,
	}
}

// Execute docs.
func (a *tagImageAction) Execute(ctx actions.ActionContext) (err error) {
	var (
		cli       *client.Client
		dockerCtx = context.Background()
	)

	defer func() {
		ctx.Status().End(err == nil)
	}()
	if cli, err = baseExecute(ctx.Status(), fmt.Sprintf("Tagging docker image (%s => %s)", a.oldImage, a.newImage)); err != nil {
		return err
	}

	if ctx.IsDryRun() {
		return nil
	}

	err = cli.ImageTag(dockerCtx, a.oldImage, a.newImage)

	return err
}
