package docker

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"gitlab.com/digitalxero/simple-actions"
)

type pushImageAction struct {
	image string
}

// NewPushImageAction docs.
func NewPushImageAction(image string) actions.Action {
	return &pushImageAction{image: image}
}

// Execute docs.
func (a *pushImageAction) Execute(ctx actions.ActionContext) (err error) {
	var (
		cli       *client.Client
		dockerCtx = context.Background()
		resp      io.ReadCloser
		data      []byte
	)

	defer func() {
		ctx.Status().End(err == nil)
	}()

	if cli, err = baseExecute(ctx.Status(), fmt.Sprintf("Pushing docker image (%s)", a.image)); err != nil {
		return err
	}

	if ctx.IsDryRun() {
		return nil
	}

	if resp, err = cli.ImagePush(dockerCtx, a.image, types.ImagePushOptions{
		All:          true,
		RegistryAuth: "e30=", // "{}"
	}); err != nil {
		return err
	}
	defer resp.Close()

	if data, err = ioutil.ReadAll(resp); err != nil {
		return err
	}

	ctx.Logger().V(3).Info(string(data))

	return nil
}
