// Package docker contains docker related actions
package docker

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/docker/cli/cli/config/configfile"
	"github.com/docker/cli/cli/config/credentials"
	cliTypes "github.com/docker/cli/cli/config/types"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"gitlab.com/digitalxero/simple-actions/term"
	"gitlab.com/digitalxero/simple-actions/log"
)

func baseExecute(status *term.Status, msg string) (cli *client.Client, err error) {
	status.Start(msg)

	return client.NewClientWithOpts(client.FromEnv)
}

func pullImage(logger log.Logger, cli *client.Client, image string) (err error) {
	var (
		output io.ReadCloser
		ctx    = context.Background()
		cfg    *configfile.ConfigFile
	)

	if _, _, err = cli.ImageInspectWithRaw(ctx, image); err != nil && !client.IsErrNotFound(err) {
		// Error inspecting, but not image not found
		return fmt.Errorf("error reading raw: %w", err)
	} else if err == nil && !strings.HasSuffix(image, ":latest") {
		// image exists so we already pulled it
		return nil
	}

	host := "docker.io"
	parts := strings.Split(image, "/")
	if strings.Contains(parts[0], ".") {
		host = parts[0]
	}
	if cfg, err = loadConfigFile(os.ExpandEnv("$HOME/.docker/config.json")); err != nil {
		return err
	}
	authCFG, _ := cfg.GetAuthConfig(host)
	encodedAuth, err := encodeAuthToBase64(&authCFG)
	logger.V(3).Infof("Docker Auth Token: %s\n", encodedAuth)
	if output, err = cli.ImagePull(context.Background(), image, types.ImagePullOptions{
		RegistryAuth:  encodedAuth,
		PrivilegeFunc: registryAuthenticationPrivilegedFunc(&authCFG),
	}); err != nil {
		return err
	}
	defer output.Close()
	outdec := json.NewDecoder(output)
	type statusLine struct {
		ID       string `json:"id"`
		Status   string `json:"status"`
		Progress string `json:"progress"`
		Error    string `json:"error"`
	}
	for {
		var line statusLine
		if err = outdec.Decode(&line); errors.Is(err, io.EOF) {
			break
		} else if err != nil {
			logger.Error(err.Error())

			continue
		}

		if line.Error != "" {
			// nolint:goerr113
			return fmt.Errorf(line.Error)
		}
		logger.V(3).Infof("%+v\n", line)
	}

	return nil
}

// encodeAuthToBase64 serializes the auth configuration as JSON base64 payload.
func encodeAuthToBase64(authConfig *cliTypes.AuthConfig) (string, error) {
	buf, err := json.Marshal(authConfig)
	if err != nil {
		return "", err
	}

	return base64.URLEncoding.EncodeToString(buf), nil
}

func registryAuthenticationPrivilegedFunc(authConfig *cliTypes.AuthConfig) types.RequestPrivilegeFunc {
	return func() (token string, err error) {
		return encodeAuthToBase64(authConfig)
	}
}

func loadConfigFile(path string) (*configfile.ConfigFile, error) {
	cfg := configfile.New(path)
	if _, err := os.Stat(path); err == nil {
		file, innerErr := os.Open(path)
		if innerErr != nil {
			return nil, innerErr
		}
		defer file.Close()
		if cfgErr := cfg.LoadFromReader(file); cfgErr != nil {
			return nil, cfgErr
		}
	} else if !os.IsNotExist(err) {
		return nil, err
	}
	if !cfg.ContainsAuth() {
		cfg.CredentialsStore = credentials.DetectDefaultStore(cfg.CredentialsStore)
	}

	return cfg, nil
}
