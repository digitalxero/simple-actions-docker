

<!--- next entry here -->

## 1.0.5
2021-02-25

### Fixes

- update deps (89049f6999b7c368f9b43af2fdf06a24d1c6a4ce)

## 1.0.4
2021-02-25

### Fixes

- update deps (4e2f53f8c3c3368ff390683eb361c28298e5f1b6)

## 1.0.3
2021-02-25

### Fixes

- update the deps (db3ba0fddb049bd5696d45c934c3babc259308c9)

## 1.0.2
2021-02-20

### Fixes

- ensure authenticated docker registries works properly as well (f8ba5a754e8281c498f6f81eab17e0f344c2fbb4)
- lint issues (b0355f426be6ef82e8dcbbe1b60e832d52d559c6)
- lint issues (f46d8eb571ade7bfde8b3f614b43da03aebb6b47)
- lint issues (0ecaac2b47a0e8f1bd62f67fdcb8b02f1f0fcf45)

## 1.0.0
2020-06-03

### Fixes

- update deps (4eb27d335be97fce28eb2e83d8666bab4484a5e6)
- add missing changelog file (0ccb86334d2c3ffeb6e6e9e6256b7166df3e339c)
- dep update (9cb5f3a5c4588205066502190080c9ccce3be117)
- dep update (bc5540458af5fb0ac71d7445f14ea7dfb7c7d4ef)
- version tags (5b54da8bab4cfbf4303de79d1fdbf32a3731880a)
- ensure the network create functions as expected even if it already exists (bd292a9671242f0c592b05e0f32fb4a3271d5223)

## 1.0.0
2020-05-26

### Fixes

- update deps (4eb27d335be97fce28eb2e83d8666bab4484a5e6)
- add missing changelog file (0ccb86334d2c3ffeb6e6e9e6256b7166df3e339c)
- dep update (9cb5f3a5c4588205066502190080c9ccce3be117)
- dep update (bc5540458af5fb0ac71d7445f14ea7dfb7c7d4ef)
- version tags (5b54da8bab4cfbf4303de79d1fdbf32a3731880a)

