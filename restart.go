package docker

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"gitlab.com/digitalxero/simple-actions"
)

type startAction struct {
	name string
}

type stopAction struct {
	name string
}

// NewStartAction docs.
func NewStartAction(name string) actions.Action {
	return &startAction{name: name}
}

// NewStopAction docs.
func NewStopAction(name string) actions.Action {
	return &stopAction{name: name}
}

// NewRestartAction docs.
func NewRestartAction(name string) actions.Actions {
	return actions.Actions{
		NewStopAction(name),
		NewStartAction(name),
	}
}

// Execute docs.
func (a *startAction) Execute(ctx actions.ActionContext) (err error) {
	var (
		cli       *client.Client
		dockerCtx = context.Background()
	)

	defer func() {
		ctx.Status().End(err == nil)
	}()
	if cli, err = baseExecute(ctx.Status(), fmt.Sprintf("Starting container %s", a.name)); err != nil {
		return err
	}

	if ctx.IsDryRun() {
		return nil
	}

	err = cli.ContainerStart(dockerCtx, a.name, types.ContainerStartOptions{})
	return err
}

// Execute docs.
func (a *stopAction) Execute(ctx actions.ActionContext) (err error) {
	var (
		cli       *client.Client
		dockerCtx = context.Background()
		timeout   = 45 * time.Second
	)

	defer func() {
		ctx.Status().End(err == nil)
	}()
	if cli, err = baseExecute(ctx.Status(), fmt.Sprintf("Stopping container %s", a.name)); err != nil {
		return err
	}

	if ctx.IsDryRun() {
		return nil
	}

	if err = cli.ContainerStop(dockerCtx, a.name, &timeout); err != nil && strings.Contains(err.Error(), "No such container") {
		// if the container does not exist, then it is obviously stopped.
		ctx.Logger().V(3).Info(err.Error())
		err = nil
	}

	return err
}
